package TestCases;

import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import automationFramework.GenericKeywords;
import baseClass.BaseClass;
import scenarios.General.HeaderAndFooters;
import scenarios.LabTests.LabTests;
import scenarios.cart.Cart;
import scenarios.checkout.Checkout;
import scenarios.contact.Contact;

@Listeners({ utilities.HtmlReport.class })
public class TestCases {
	private BaseClass obj;

	////////////////////////////////////////////////////////////////////////////////
	// Function Name :
	// Purpose :
	// Parameters :
	// Return Value :
	// Created by :
	// Created on :
	// Remarks :
	/////////////////////////////////////////////////////////////////////////////////

	private void TestStart(String testCaseName) {

		obj.currentTestCaseName = testCaseName;
		obj.setEnvironmentTimeouts();
		obj.reportStart();
		obj.iterationCount.clear();
		obj.iterationCountInTextData();

	}

	@BeforeClass
	@Parameters({ "selenium.machinename", "selenium.host", "selenium.port", "selenium.browser", "selenium.os",
			"selenium.browserVersion", "selenium.osVersion", "TestData.SheetNumber" })
	public void precondition(String machineName, String host, String port, String browser, String os,
			String browserVersion, String osVersion, String sheetNo) {
		obj = new BaseClass();
		if (os.contains("Android")) {
			obj.startServer(host, port);
			obj.waitTime(10);
		}
		obj.setup(machineName, host, port, browser, os, browserVersion, osVersion, sheetNo);
	}

	@Test(alwaysRun = true)
	public void General_MHN_TS_001(Method method) {
		TestStart(method.getName());
		HeaderAndFooters headersAndFooters = new HeaderAndFooters(obj);
		headersAndFooters.dataRowNo = Integer.parseInt(headersAndFooters.iterationCount.get(0).toString());
		headersAndFooters.verifyApplicationLaunchedSuccessfully();
		obj.testFailure = headersAndFooters.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void General_MHN_TS_002_009(Method method) {
		TestStart(method.getName());
		HeaderAndFooters headersAndFooters = new HeaderAndFooters(obj);
		headersAndFooters.dataRowNo = Integer.parseInt(headersAndFooters.iterationCount.get(0).toString());
		headersAndFooters.verifyHeader();
		obj.testFailure = headersAndFooters.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void General_MHN_TS_003(Method method) {
		TestStart(method.getName());
		HeaderAndFooters headersAndFooters = new HeaderAndFooters(obj);
		headersAndFooters.dataRowNo = Integer.parseInt(headersAndFooters.iterationCount.get(0).toString());
		headersAndFooters.verifyFooter();
		obj.testFailure = headersAndFooters.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void General_MHN_TS_004(Method method) {
		TestStart(method.getName());
		HeaderAndFooters headersAndFooters = new HeaderAndFooters(obj);
		headersAndFooters.dataRowNo = Integer.parseInt(headersAndFooters.iterationCount.get(0).toString());
		headersAndFooters.verifyLogoLink();
		obj.testFailure = headersAndFooters.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void PGP_MHN_TS_018(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.PGPtoPDP_NavigationVerify();
		obj.testFailure = miniCart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void PDP_MHN_TS_024(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.updateQuantity_AddToCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void MiniCart_MHN_TS_028(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyDeleteProductFromMiniCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void MiniCart_MHN_TS_025(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyMiniCartProductNumberDisplayed();
		obj.testFailure = miniCart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void MiniCart_MHN_TS_026(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.modifyQuantityinMiniCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void MiniCart_MHN_TS_027_030(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.editProductFromMiniCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void MiniCart_MHN_TS_029(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyCartSubTotal();
		obj.testFailure = miniCart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void MiniCart_MHN_TS_031(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyViewAndEditCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MiniCart_MHN_TS_032(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyGoToCheckout();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_MHN_TS_033(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.addProductToCart_PopUpverify();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_MHN_TS_034(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyDeleteProductFromCart();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_MHN_TS_035(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyUpdateQuantityInCart();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_MHN_TS_036(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.clearAllProducts();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_MHN_TS_037(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.checkContinueShopping();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_MHN_TS_039(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyShippingAddress();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_048(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidShippingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_047_049(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ValidShippingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_052_053(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidCreditCardDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_054(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifyBillingAddressAutofilledSameAsShippingAddress();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_055(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifyBillingFormDisplayed();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_057(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidBillingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_056(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ValidBillingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_058(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifyNavigationToShippingPageFromPayments();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_060_059(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ApplyInvalidAndValidCoupon();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_MHN_TS_062(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_CompleteCheckoutWithValidDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Contact_MHN_TS_067(Method method) {
		TestStart(method.getName());
		Contact contact = new Contact(obj);
		contact.dataRowNo = Integer.parseInt(contact.iterationCount.get(0).toString());
		contact.submitInvalidDetails();
		obj.testFailure = contact.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Contact_MHN_TS_066(Method method) {
		TestStart(method.getName());
		Contact contact = new Contact(obj);
		contact.dataRowNo = Integer.parseInt(contact.iterationCount.get(0).toString());
		contact.submitValidDetails();
		obj.testFailure = contact.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void LabTests_MHN_TS_071(Method method) {
		TestStart(method.getName());
		LabTests labTests = new LabTests(obj);
		labTests.dataRowNo = Integer.parseInt(labTests.iterationCount.get(0).toString());
		labTests.verifyInvalidSearch();
		obj.testFailure = labTests.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void LabTests_MHN_TS_070(Method method) {
		TestStart(method.getName());
		LabTests labTests = new LabTests(obj);
		labTests.dataRowNo = Integer.parseInt(labTests.iterationCount.get(0).toString());
		labTests.verifyValidSearch();
		obj.testFailure = labTests.testFailure;
		TestEnd();
	}

	@AfterClass
	public void closeSessions() {
		obj.closeAllSessions();
	}

	private void TestEnd() {
		obj.waitTime(1);
		if (obj.testFailure) {
			GenericKeywords.testFailed();
		}
	}

	@BeforeMethod
	public void before() {
		obj.testFailure = false;
	}

}
