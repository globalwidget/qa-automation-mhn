package scenarios.cart;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MiniCart;
import pages.ShoppingCart;

public class Cart extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	Cart_AllProducts cart_AllProducts;
	HeaderAndFooters headerAndfooter;
	ShoppingCart shoppingCart;
	MiniCart miniCart;
	private boolean status = false;

	String password;

	public Cart(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		headerAndfooter = new HeaderAndFooters(obj);
		shoppingCart = new ShoppingCart(obj);
		miniCart = new MiniCart(obj);
	}

	/*
	 * TestCaseid : Cart and PGP Description : To add a product to the cart.
	 */
	public void addProductToCart_PopUpverify() {
		try {
			String productName;
			float cartSubTotal;
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			productName = cart_AllProducts.getProductTitle_PDP();
			cart_AllProducts.chooseCapsuleStrength();
			cartSubTotal = cart_AllProducts.getProductPrice();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.checkDetails_AddToCart(productName, 1, cartSubTotal);
			cart_AllProducts.closeProceedToCartPopUp();
		} catch (Exception e) {
			testStepFailed("Add To Cart could not be done");
		}
		if (obj.testFailure || homePage.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure
				|| miniCart.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to delete the
	 * product from the cart.
	 */
	public void verifyDeleteProductFromCart() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			String productRemoved;
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickProceedToCart();
			productRemoved = shoppingCart.clickOnRemoveProduct();
			headerAndfooter.verifyNavigation("emptyCart");
			testStepInfo("The product - " + productRemoved + "was removed from the Cart");

		} catch (Exception e) {
			testStepFailed("Deleting a product from Shopping Cart could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to update the
	 * quantity of the products in the cart page
	 */
	public void verifyUpdateQuantityInCart() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			int quantity_input = 10;
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickProceedToCart();
			shoppingCart.updateCartQuantity(quantity_input);
			shoppingCart.clickUpdateCart();
			shoppingCart.checkCartQuantity(quantity_input);
		} catch (Exception e) {
			testStepFailed("The user is not able to modify the quantity of the product in the Shopping Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to clear all the
	 * products from cart
	 */
	public void clearAllProducts() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectSecondProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickProceedToCart();
			shoppingCart.clickClearCart();
			headerAndfooter.verifyNavigation("emptyCart");
			headerAndfooter.goTo_Home();
			testStepInfo("All products were removed from the Cart");

		} catch (Exception e) {
			testStepFailed("Deleting a product from Shopping Cart could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || shoppingCart.testFailure
				|| homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify clicking on 'Continue Shopping' button
	 * is taking the user to home page
	 */
	public void checkContinueShopping() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickProceedToCart();
			shoppingCart.clickContinueShoppingCart();
			headerAndfooter.verifyNavigation("home");
			testStepInfo("Function of Continue Shopping button was verified");

		} catch (Exception e) {
			testStepFailed("Deleting a product from Shopping Cart could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || shoppingCart.testFailure
				|| homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to change the
	 * shipping address in cart page
	 */
	public void verifyShippingAddress() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickViewAndEditCart();
			}
			shoppingCart.fillDetialsShipping("California", 33634);
			waitTime(3);
			shoppingCart.checkTax(false);
			shoppingCart.fillDetialsShipping("Florida", 0);
			waitTime(3);
			shoppingCart.checkTax(true);

		} catch (Exception e) {
			testStepFailed("Deleting a product from Shopping Cart could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || shoppingCart.testFailure
				|| homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
