package scenarios.LabTests;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;

public class LabTests extends ApplicationKeywords {
	BaseClass obj;
	pages.LabTestingResults labTests;
	HeaderAndFooters headerAndFooter;
	Cart_AllProducts cart_AllProducts;
	HomePage homePage;
	private boolean status = false;

	String password;

	public LabTests(BaseClass obj) {
		super(obj);
		this.obj = obj;
		labTests = new pages.LabTestingResults(obj);
		headerAndFooter = new HeaderAndFooters(obj);
		homePage = new HomePage(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
	}

	/*
	 * TestCaseid : Lab Test - Batch Search Description : Verify proper error
	 * message is displayed for the invalid batch search
	 */
	public void verifyInvalidSearch() {
		try {

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndFooter.goTo_Labtests();
			labTests.clickSearchIcon();
			labTests.enterBatchNumber("  ");
			labTests.clickSearchIcon();
			labTests.verifyNullResult();
		} catch (Exception e) {
			testStepFailed("Could not verify lab test results using the invalid batch number");
		}
		if (obj.testFailure || labTests.testFailure || cart_AllProducts.testFailure || homePage.testFailure
				|| headerAndFooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Lab Test - Batch Search Description : Verify if the user is able
	 * to search for lab test results using the valid batch number
	 */
	public void verifyValidSearch() {
		try {
			String batchNumber = retrieve("batchNumber");
			if (GOR.OfferPopUpHandled == false) {
				headerAndFooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndFooter.goTo_Labtests();
			labTests.enterBatchNumber(batchNumber);
			labTests.clickSearchIcon();
			labTests.checkViewResults();
		} catch (Exception e) {
			testStepFailed("Could not verify lab test results using the valid batch number");
		}
		if (obj.testFailure || headerAndFooter.testFailure || labTests.testFailure || homePage.testFailure
				|| cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}