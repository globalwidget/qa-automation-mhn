package scenarios.miniCart;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.Checkout;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.ShoppingCart;

public class MiniCart extends ApplicationKeywords {
	BaseClass obj;
	Cart_AllProducts cart_AllProducts;
	pages.MiniCart miniCart;
	HeaderAndFooters headerAndfooter;
	ShoppingCart shoppingCart;
	Checkout checkout;
	HomePage homePage;
	private boolean status = false;

	public MiniCart(BaseClass obj) {
		super(obj);
		this.obj = obj;
		cart_AllProducts = new Cart_AllProducts(obj);
		miniCart = new pages.MiniCart(obj);
		headerAndfooter = new HeaderAndFooters(obj);
		shoppingCart = new ShoppingCart(obj);
		checkout = new Checkout(obj);
		homePage = new HomePage(obj);
	}

	/*
	 * TestCaseid : PGP Description : Verify clicking on the products from PGP page
	 * is taking the user to PDP page.
	 */
	public void PGPtoPDP_NavigationVerify() {
		try {
			String productName_PGP = null;
			String productName_PDP = null;
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.goTo_Products(0, 0);
			productName_PGP = cart_AllProducts.getProductName_PGP();
			cart_AllProducts.selectFirstProduct();
			productName_PDP = cart_AllProducts.getProductTitle_PDP();
			if (productName_PGP.contains(productName_PDP)) {
				testStepInfo("Clicking on a product in PGP page opened the relevant PDP page");
			} else {
				testStepFailed("Clicking on a product in PGP page opened the relevant PDP page");
			}

		} catch (Exception e) {
			testStepFailed("Clicking on a product in PGP page opened the relevant PDP page");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PDP Description : Verify if the user is able to modify the
	 * quantity of the product and add to cart.
	 */
	public void updateQuantity_AddToCart() {
		try {
			int productQuantity_PDP = 10;
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.updateProductQuantity(productQuantity_PDP);
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickProceedToCart();
			int productQuantity = shoppingCart.getProductQuantity();
			headerAndfooter.goTo_Home();
			if (productQuantity == productQuantity_PDP) {
				testStepInfo("The product count updated in the PDP page is reflected in the Shopping Cart");
			} else {
				testStepFailed("The product count updated in the PDP page is not reflected in the Shopping Cart");
			}

		} catch (Exception e) {
			testStepFailed("The product count updated in the PDP page is not reflected in the Shopping Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || shoppingCart.testFailure
				|| homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Mini Cart Description : Verify the mini cart numbers are getting
	 * updated / displayed as per the number of products in cart.
	 */
	public void verifyMiniCartProductNumberDisplayed() {
		try {
			int quantity;
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			quantity = miniCart.getProductQuantityDisplayed();
			if (quantity != 1) {
				testStepFailed("The number products added does not match with that displayed in the Mini Cart");
			}
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectSecondProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.updateProductQuantity(3);
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			quantity = miniCart.getProductQuantityDisplayed();
			if (quantity != 4) {
				testStepFailed("The number products added does not match with that displayed in the Mini Cart");
			}
		} catch (Exception e) {
			testStepFailed("The product count updated in the PDP page is not reflected in the Shopping Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Mini Cart Description : Verify if the user is able to modify the
	 * quantity of the product in the mini cart.
	 */
	public void modifyQuantityinMiniCart() {
		try {
			int quantity_MiniCart = 10;
			int quantity_shoppingCart;
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			headerAndfooter.clickOnMiniCart();
			miniCart.updateCartQuantity(quantity_MiniCart);
			miniCart.clickUpdateProductCountIcon();
			miniCart.clickViewAndEditCart();
			quantity_shoppingCart = shoppingCart.getProductQuantity();
			headerAndfooter.goTo_Home();
			if (quantity_shoppingCart == quantity_MiniCart) {
				testStepInfo("The user is able to modify the quantity of the product in the Mini Cart");
			} else {
				testStepFailed("The user is not able to modify the quantity of the product in the Mini Cart");
			}

		} catch (Exception e) {
			testStepFailed("The user is not able to modify the quantity of the product in the Mini Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Mini Cart Description : Verify if the user is able to edit the
	 * item from mini cart.
	 */
	public void editProductFromMiniCart() {
		try {
			int quantity_MiniCart;
			int quantity_PDP = 9;
			String productName_PDP;
			String productName_MiniCart;
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			headerAndfooter.clickOnMiniCart();
			miniCart.selectFirstProduct();
			productName_PDP = cart_AllProducts.getProductTitle_PDP();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.updateProductQuantity(quantity_PDP);
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			headerAndfooter.clickOnMiniCart();
			productName_MiniCart = miniCart.getFirstProductTitle();
			if (productName_PDP.contains(productName_MiniCart)) {
				testStepInfo("The product added is present in the Mini Cart");
			} else {
				testStepFailed("The product added is not present in the Mini Cart");
			}
			quantity_MiniCart = quantity_PDP + 1;
			miniCart.checkCartQuantity(quantity_MiniCart);
		} catch (Exception e) {
			testStepFailed("The user is not able to edit the item in the Mini Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Mini Cart Description : Verify if the user is able to delete the
	 * product from mini cart.
	 */
	public void verifyDeleteProductFromMiniCart() {
		try {
			String productRemoved;
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			headerAndfooter.clickOnMiniCart();
			productRemoved = miniCart.clickOnRemoveProduct();
			miniCart.verifyEmptyMiniCart();
			testStepInfo("The product " + productRemoved + "was removed from the Mini Cart");
		} catch (Exception e) {
			testStepFailed("Could not verify deleting a product from the Mini Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Mini Cart Description : Verify if the cart sub total is getting
	 * updated / displayed as per the action made.
	 */
	public void verifyCartSubTotal() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			float firstProductPrice = cart_AllProducts.getProductPrice();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			waitTime(3);
			headerAndfooter.clickOnMiniCart();
			firstProductPrice = miniCart.verifyProductPriceInMiniCart(firstProductPrice);
			miniCart.verifyCartSubTotalInMiniCart(firstProductPrice);
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectSecondProduct();
			float secondProductPrice = cart_AllProducts.getProductPrice();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			waitTime(3);
			headerAndfooter.clickOnMiniCart();
			secondProductPrice = miniCart.verifyProductPriceInMiniCart(secondProductPrice);
			miniCart.verifyCartSubTotalInMiniCart(firstProductPrice + secondProductPrice);

		} catch (Exception e) {
			testStepFailed("The product count updated in the PDP page is not reflected in the Shopping Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Mini Cart Description : Verify clicking on 'View & Edit Cart' is
	 * taking the user to cart page
	 */
	public void verifyViewAndEditCart() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.closeProceedToCartPopUp();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.clickViewAndEditCart();
			shoppingCart.verifyShoppingCartHeader();
			headerAndfooter.goTo_Home();
			testStepInfo("The function of View & Edit Cart was verified");

		} catch (Exception e) {
			testStepFailed("The function of View & Edit Cart could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Mini Cart Description : Verify clicking on 'Go to checkout' from
	 * mini cart is taking the user to checkout page with specific products
	 */
	public void verifyGoToCheckout() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Home();
			waitTime(3);
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			String productName_PDP;
			headerAndfooter.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			productName_PDP = cart_AllProducts.getProductTitle_PDP();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			headerAndfooter.clickOnMiniCart();
			miniCart.clickGoToCheckout();
			checkout.verifyShippingHeader();
			checkout.verifyPresenceOfExpectedProduct(productName_PDP);
			headerAndfooter.goTo_Home();
			testStepInfo("The function of Go To Checkout was verified");
		} catch (Exception e) {
			testStepFailed("The function of Go To Checkout could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| checkout.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
