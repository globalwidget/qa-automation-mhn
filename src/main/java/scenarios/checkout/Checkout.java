package scenarios.checkout;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MiniCart;
import pages.ShoppingCart;

public class Checkout extends ApplicationKeywords {
	BaseClass obj;
	Cart_AllProducts cart_AllProducts;
	ShoppingCart shoppingCart;
	pages.Checkout checkout;
	HeaderAndFooters headerAndfooter;
	MiniCart miniCart;
	HomePage homePage;

	private boolean status = false;
	HashMap<String, String> billingData;
	HashMap<String, String> shippingData;

	public Checkout(BaseClass obj) {
		super(obj);
		this.obj = obj;
		cart_AllProducts = new Cart_AllProducts(obj);
		shoppingCart = new ShoppingCart(obj);
		checkout = new pages.Checkout(obj);
		headerAndfooter = new HeaderAndFooters(obj);
		miniCart = new MiniCart(obj);
		homePage = new HomePage(obj);
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is not able to
	 * place the order with any invalid details in the checkout page.
	 */
	public void checkout_InvalidShippingDetails() {
		try {
			String invalidEmailAddress = retrieve("invalidEmailAddress");
			String invalidShippingPostCode = retrieve("invalidShippingPostCode");

			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", invalidShippingPostCode);
			shippingData.put("email_data", invalidEmailAddress);

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.verifyRelevantMessage("Invalid Postal Code");
			checkout.verifyRelevantMessage("Invalid Email");
			checkout.clickNext();
			checkout.verifyRelevantMessage("Missing Shipping Method");
			checkout.ClearDetailsShipping();
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.VerifyEmptyFieldErrors(true);
		} catch (Exception e) {
			testStepFailed("Placing Order with Invalid Shipping Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Verify if the user is able to fill shipping details successfully
	 * and suggested address is displayed.
	 */
	public void checkout_ValidShippingDetails() {
		try {
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.verifyRelevantMessage("Suggested Address");
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.verifyRelevantMessage("Review and Payments");

		} catch (Exception e) {
			testStepFailed(
					"Successful filling of valid shipping details and display of suggested address could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is not able to
	 * place the order with any invalid card details in the checkout page.
	 */
	public void checkout_InvalidCreditCardDetails() {
		try {
			String invalidcardNumber = retrieve("invalidCardNumber");
			String invalidcardVerificationNumber = retrieve("invalidCardVerificationNumber");
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.selectCreditCardPayment();
			checkout.clickPlaceOrder();
			checkout.VerifyEmptyCardDetailsErrors();
			checkout.fillDetailsforCreditCard(invalidcardNumber, null, null, invalidcardVerificationNumber);
			checkout.clickPlaceOrder();
			checkout.verifyRelevantMessage("Invalid Card Number");
			checkout.verifyRelevantMessage("Invalid Verification Number");

		} catch (Exception e) {
			testStepFailed("Error messages for Invalid Card Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout_MHN_TS_054 Description : Verify the given shipping
	 * address are getting displayed as billing address when the
	 * "My billing and shipping address are the same" check box is selected.
	 */
	public void checkout_verifyBillingAddressAutofilledSameAsShippingAddress() {
		try {
			shippingData = new HashMap<String, String>();
			String firstName_data = retrieve("firstName_shipping");
			String lastName_data = retrieve("lastName_shipping");
			String company_data = retrieve("company");
			String billingCountry_data = "United States";
			String streetAddressFirstLine_data = retrieve("streetAddressFirstLine_shipping");
			String streetAddressSecondLine_data = retrieve("streetAddressSecondLine_shipping");
			String streetAddressThirdLine_data = retrieve("streetAddressThirdLine_shipping");
			String city_data = retrieve("city_shipping");
			String state_data = retrieve("state_shipping");
			String phoneNumber_data = retrieve("phoneNumber");
			String postCode_data = retrieve("postCode");
			String email_data = retrieve("email");

			shippingData.put("firstName_data", firstName_data);
			shippingData.put("lastName_data", lastName_data);
			shippingData.put("company_data", company_data);
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", streetAddressFirstLine_data);
			shippingData.put("streetAddressSecondLine_data", streetAddressSecondLine_data);
			shippingData.put("streetAddressThirdLine_data", streetAddressThirdLine_data);
			shippingData.put("city_data", city_data);
			shippingData.put("state_data", state_data);
			shippingData.put("phoneNumber_data", phoneNumber_data);
			shippingData.put("postCode_data", postCode_data);
			shippingData.put("email_data", email_data);

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.selectCreditCardPayment();
			checkout.verifyDataInBillingAddress(firstName_data, lastName_data, streetAddressFirstLine_data,
					streetAddressSecondLine_data, streetAddressThirdLine_data, city_data, state_data,
					Integer.parseInt(postCode_data), billingCountry_data, phoneNumber_data);

		} catch (Exception e) {
			testStepFailed(
					"The given Shipping Address getting displayed when the \"My billing and shipping address are the same\" check box is selected could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout_MHN_TS_055 Description : Verify the billing address
	 * form is getting displayed when the
	 * "My billing and shipping address are the same" check box is not selected.
	 */
	public void checkout_verifyBillingFormDisplayed() {
		try {
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.selectCreditCardPayment();
			checkout.clickBillingShippingAddressSame();
			checkout.verifyBillingFormDisplayed();
		} catch (Exception e) {
			testStepFailed("Billing Address form not displayed when relevant check box is not selected");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout_MHN_TS_057 Description : To Verify error messages are
	 * displayed for invalid details in the billing address form.
	 */
	public void checkout_InvalidBillingDetails() {
		try {
			String invalidBillingPostCode = retrieve("invalidBillingPostCode");

			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			billingData = new HashMap<String, String>();
			billingData.put("firstName_data", retrieve("firstName_billing"));
			billingData.put("lastName_data", retrieve("lastName_billing"));
			billingData.put("company_data", retrieve("company_billing"));
			billingData.put("billingCountry_data", null);
			billingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_billing"));
			billingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_billing"));
			billingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_billing"));
			billingData.put("city_data", retrieve("city_billing"));
			billingData.put("state_data", retrieve("state_billing"));
			billingData.put("phoneNumber_data", retrieve("phoneNumber_billing"));
			billingData.put("postCode_data", invalidBillingPostCode);

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.selectCreditCardPayment();
			checkout.clickBillingShippingAddressSame();
			checkout.clickUpdate();
			checkout.VerifyEmptyFieldErrors(false);
			checkout.fillDetailsBilling(billingData);
			checkout.verifyRelevantMessage("Invalid Postal Code");
		} catch (Exception e) {
			testStepFailed("Placing Order with Invalid Billing Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout_MHN_TS_056 Description : To Verify if the user is able
	 * to fill the billing address with all valid details.
	 */
	public void checkout_ValidBillingDetails() {
		try {
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			billingData = new HashMap<String, String>();
			billingData.put("firstName_data", retrieve("firstName_billing"));
			billingData.put("lastName_data", retrieve("lastName_billing"));
			billingData.put("company_data", retrieve("company_billing"));
			billingData.put("billingCountry_data", null);
			billingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_billing"));
			billingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_billing"));
			billingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_billing"));
			billingData.put("city_data", retrieve("city_billing"));
			billingData.put("state_data", retrieve("state_billing"));
			billingData.put("phoneNumber_data", retrieve("phoneNumber_billing"));
			billingData.put("postCode_data", retrieve("billingPostCode"));

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.selectCreditCardPayment();
			checkout.clickBillingShippingAddressSame();
			checkout.fillDetailsBilling(billingData);
			checkout.clickUpdate();
		} catch (Exception e) {
			testStepFailed("Successful entry of Valid Billing Details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout_MHN_TS_058 Description : To Verify if the user is able
	 * to navigate to Checkout - Shipping page from Review & Payments page.
	 */
	public void checkout_verifyNavigationToShippingPageFromPayments() {
		try {
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.clickShippingProgressBar();
			checkout.verifyShippingHeader();
			testStepInfo("Successfully navigated from Payments Tab to Shipping Tab");
		} catch (Exception e) {
			testStepFailed("Navigation to Shipping Tab from Payments could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout_MHN_TS_060_059 Description : To validate error message
	 * is displayed upon submitting invalid coupon and if user is able to apply
	 * valid coupon in the checkout.
	 */
	public void checkout_ApplyInvalidAndValidCoupon() {
		try {

			String invalidCouponCode = retrieve("invalidCouponCode");
			String validCouponCode = retrieve("validCouponCode");
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.clickApplyDiscountCodeHeader();
			checkout.clickApplyDiscount();
			checkout.verifyRelevantMessage("Discount Code Missing");
			checkout.enterDiscountCode(invalidCouponCode);
			checkout.clickApplyDiscount();
			checkout.verifyRelevantMessage("Invalid Coupon");
			checkout.enterDiscountCode(validCouponCode);
			checkout.clickApplyDiscount();
			checkout.clickCancel_DiscountCoupon();
			testStepInfo("Valid Coupon was applied successfully");
		} catch (Exception e) {
			testStepFailed(
					"Could not verify errors for invalid coupon data and successful applying of valid coupon data");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout_MHN_TS_062 Description : To Verify if the user is able
	 * to checkout the products with valid credit card details as a guest user.
	 */
	public void checkout_CompleteCheckoutWithValidDetails() {
		try {
			String cardNumber = retrieve("cardNumber");
			String expirationMonth = retrieve("expirationMonth");
			String expirationYear = retrieve("expirationYear");
			String cardVerificationNumber = retrieve("cardVerificationNumber");
			shippingData = new HashMap<String, String>();
			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseCapsuleStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickProceedToCart();
				shoppingCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickGoToCheckout();
			}
			checkout.fillDetailsShipping(shippingData);
			checkout.clickFixedUnderShippingMethods();
			checkout.clickNext();
			checkout.selectCreditCardPayment();
			checkout.fillDetailsforCreditCard(cardNumber, expirationMonth, expirationYear, cardVerificationNumber);
			checkout.clickPlaceOrder();
			checkout.verifySuccessMessage();

		} catch (Exception e) {
			testStepFailed("Checkout of order with valid credit card details could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| checkout.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
