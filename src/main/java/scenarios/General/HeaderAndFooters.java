package scenarios.General;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HomePage;
import pages.MiniCart;
import pages.ShoppingCart;

public class HeaderAndFooters extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	pages.HeaderAndFooters headerAndFooters;
	Cart_AllProducts cart_AllProducts;
	MiniCart miniCart;
	ShoppingCart shoppingCart;
	private boolean status = false;

	String password;

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		miniCart = new MiniCart(obj);
		headerAndFooters = new pages.HeaderAndFooters(obj);
		shoppingCart = new ShoppingCart(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
	}

	/*
	 * TestCaseid : General Description : Verify if the user is able to launch the
	 * MyHempNow application successfully
	 */
	public void verifyApplicationLaunchedSuccessfully() {
		try {

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndFooters.goTo_Home();
			homePage.verifyPresenceOfSectionBuilderBlock();
			testStepInfo("MyHempNow application was launched successfully");
		} catch (Exception e) {
			testStepFailed("MyHempNow application was not launched successfully");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || cart_AllProducts.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General Description : Verify all the header menus are navigating
	 * to the respective pages
	 */
	public void verifyHeader() {
		try {

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.verifyNavigation("home");
			headerAndFooters.goTo_Products(0, 0);
			headerAndFooters.verifyNavigation("shopAll");
			headerAndFooters.goTo_Products(1, 0);
			headerAndFooters.verifyNavigation("shopAll");
			headerAndFooters.goTo_Products(2, 0);
			headerAndFooters.verifyNavigation("topicals");
			headerAndFooters.goTo_Products(3, 0);
			headerAndFooters.verifyNavigation("edibles");
			headerAndFooters.goTo_Products(3, 1);
			headerAndFooters.verifyNavigation("gummies");
			headerAndFooters.goTo_Products(3, 2);
			headerAndFooters.verifyNavigation("capsules");
			headerAndFooters.goTo_Products(4, 0);
			headerAndFooters.verifyNavigation("oils");

			headerAndFooters.goTo_Labtests();
			headerAndFooters.verifyNavigation("labTests");

			headerAndFooters.goTo_Contact();
			headerAndFooters.verifyNavigation("contact");

			headerAndFooters.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndFooters.verifyNavigation("emptyMiniCart");
			headerAndFooters.goTo_Products(0, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseCapsuleStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.closeProceedToCartPopUp();
			headerAndFooters.clickOnMiniCart();
			headerAndFooters.verifyNavigation("miniCartWithProducts");
			miniCart.clickViewAndEditCart();
			headerAndFooters.verifyNavigation("shoppingCart");
			shoppingCart.clickProceedToCheckOut();
			headerAndFooters.verifyNavigation("checkout");

		} catch (Exception e) {
			testStepFailed("Header Navigation could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure || cart_AllProducts.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || homePage.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General Description : Verify all the footer menus are navigating
	 * to the respective pages
	 */
	public void verifyFooter() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			homePage.clickOnHempBombsLogo();
			headerAndFooters.Verifypresence_PhoneNumber();
			headerAndFooters.navigateFooter("home");
			headerAndFooters.verifyNavigation("home");
			headerAndFooters.navigateFooter("shop");
			headerAndFooters.verifyNavigation("shopAll");
			homePage.clickOnHempBombsLogo();
			headerAndFooters.navigateFooter("hempGummies");
			headerAndFooters.verifyNavigation("gummies");
			homePage.clickOnHempBombsLogo();
			headerAndFooters.navigateFooter("hempCapsules");
			headerAndFooters.verifyNavigation("capsules");
			homePage.clickOnHempBombsLogo();
			headerAndFooters.navigateFooter("hempOils");
			headerAndFooters.verifyNavigation("oils");
			homePage.clickOnHempBombsLogo();
			headerAndFooters.navigateFooter("hempTopicals");
			headerAndFooters.verifyNavigation("topicals");
			homePage.clickOnHempBombsLogo();

			headerAndFooters.navigateFooter("sendUsMessage");
			headerAndFooters.verifyNavigation("contact");
			homePage.clickOnHempBombsLogo();

//			headerAndFooters.navigateFooter("createAnAccount");
//			headerAndFooters.verifyNavigation("login");
//			homePage.clickOnHempBombsLogo();
//			headerAndFooters.navigateFooter("login");
//			headerAndFooters.verifyNavigation("login");
//			homePage.clickOnHempBombsLogo();
//			headerAndFooters.navigateFooter("myAccount");
//			headerAndFooters.verifyNavigation("login");
//			homePage.clickOnHempBombsLogo();
//			headerAndFooters.navigateFooter("myAccount");
//			headerAndFooters.verifyNavigation("login");
//			homePage.clickOnHempBombsLogo();
//			headerAndFooters.navigateFooter("myOrders");
//			headerAndFooters.verifyNavigation("login");
//			homePage.clickOnHempBombsLogo();

			headerAndFooters.navigateFooter("shipping");
			headerAndFooters.verifyNavigation("shipping");
			homePage.clickOnHempBombsLogo();
			headerAndFooters.navigateFooter("refund");
			headerAndFooters.verifyNavigation("refund");
			homePage.clickOnHempBombsLogo();
			headerAndFooters.navigateFooter("terms");
			headerAndFooters.verifyNavigation("terms");
			homePage.clickOnHempBombsLogo();
			headerAndFooters.navigateFooter("privacy");
			headerAndFooters.verifyNavigation("privacy");
			homePage.clickOnHempBombsLogo();
			headerAndFooters.navigateFooter("ccpa");
			headerAndFooters.verifyNavigation("ccpa");

		} catch (Exception e) {
			testStepFailed("Footer Navigation could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure || homePage.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General Description : Verify clicking on logo from any page
	 * should take the user to the home page
	 */
	public void verifyLogoLink() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndFooters.goTo_Products(0, 0);
			homePage.clickOnHempBombsLogo();
			homePage.verifyPresenceOfSectionBuilderBlock();
			testStepInfo("MyHempNow application was launched successfully");
		} catch (Exception e) {
			testStepFailed("MyHempNow application was not launched successfully");
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure || homePage.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
