package scenarios.contact;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.ContactUs;
import pages.HeaderAndFooters;
import pages.HomePage;

public class Contact extends ApplicationKeywords {
	BaseClass obj;
	HeaderAndFooters headerAndFooters;
	ContactUs contactUs;
	Cart_AllProducts cart_AllProducts;
	HomePage homePage;
	private boolean status = false;

	String password;

	public Contact(BaseClass obj) {
		super(obj);
		this.obj = obj;
		headerAndFooters = new HeaderAndFooters(obj);
		contactUs = new ContactUs(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		homePage = new HomePage(obj);
	}

	/*
	 * TestCaseid : Contact Description : Verify proper error message is displayed
	 * for the invalid fields in the form
	 */
	public void submitInvalidDetails() {
		try {

			String InvalidEmail = retrieve("invalidEmail");
			String Invalidphone = retrieve("invalidPhone");

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndFooters.goTo_Contact();
			contactUs.clickSend();
			contactUs.checkEmptyFieldErrors();
			headerAndFooters.goTo_Contact();
			contactUs.fillDetails(" ", Invalidphone, InvalidEmail, " ", " ");
			contactUs.clickSend();
			contactUs.checkInvalidDataErrors();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || contactUs.testFailure || headerAndFooters.testFailure || homePage.testFailure
				|| cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Contact Description :Verify if the user is able to submit the
	 * contact form
	 */
	public void submitValidDetails() {
		try {
			String name = retrieve("name");
			String phone = retrieve("phone");
			String email = retrieve("email");
			String business = retrieve("business");
			String message = retrieve("message");

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0, 0);
				cart_AllProducts.selectFirstProduct();
				homePage.closeOfferPopup();
			}

			headerAndFooters.goTo_Contact();
			contactUs.fillDetails(name, phone, email, business, message);
			contactUs.clickSend();
			contactUs.verifySuccessMessage();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || contactUs.testFailure || headerAndFooters.testFailure || homePage.testFailure
				|| cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
