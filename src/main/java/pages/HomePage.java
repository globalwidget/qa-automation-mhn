package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class HomePage extends ApplicationKeywords {

	private static final String closeOfferPopup = "Close Offer Pop up #xpath=//div[contains(@data-testid,'POP')]//button[contains(@class,'close')]";
	private static final String homeFooterLink = "Home Footer Link #xpath=//footer//a[contains(text(),'HOME')]";
	private static final String slide_HomePage = "Home Banner Slide #xpath=//div[contains(@id,'slide')]";
	private static final String myHempNowLogo = "HempBombs Logo #xpath=//img[@alt='MyHempNow Logo']";

	public HomePage(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of the expected product in the cart
	 * 
	 */
	public void verifyPresenceOfSectionBuilderBlock() {
		try {
			scrollToViewElement(slide_HomePage);
			if (isElementDisplayed(slide_HomePage)) {
				highLighterMethod(slide_HomePage);
				testStepInfo("HomePage of MyhempNow application is displayed");
			} else
				testStepFailed("HomePage of MyhempNow application is not displayed");
		} catch (Exception e) {
			testStepFailed("HomePage of MyhempNow application is not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on HempBombs Logo
	 * 
	 */
	public void clickOnHempBombsLogo() {
		try {
			if (isElementPresent(myHempNowLogo)) {
				clickOn(myHempNowLogo);
			} else
				testStepFailed("HempBombs Logo is not present");
		} catch (Exception e) {
			testStepFailed("HempBombs Logo could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to handle the offer pop-up
	 */
	public void closeOfferPopup() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				scrollToViewElement(homeFooterLink);
				waitTime(2);
				if (isElementPresent(closeOfferPopup)) {
					GOR.OfferPopUpHandled = true;
					clickOn(closeOfferPopup);
				} else {
					testStepInfo("Offer pop up not present");
				}
			} else
				testStepInfo("Offer Popup already handled");
		} catch (Exception e) {
			testStepInfo("Offer pop up not present");
		}
	}
}
