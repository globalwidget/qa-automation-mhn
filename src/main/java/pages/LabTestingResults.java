package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class LabTestingResults extends ApplicationKeywords {

	private static final String enterBatchNumber = "BatchNumber searchBox #xpath=//input[@id='hb-lab-input']";
	private static final String searchIcon = "Search Icon #xpath=//button[@id='hb-lab-button']";
	private static final String nullResultImage = "Null Result Image #xpath=//div[@id='hb-lab-search-null']//img";
	private static final String viewResults = "View Results #xpath=//p/following-sibling::a[contains(text(),'Results')]";

	public LabTestingResults(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to open Lab Tests page.
	 * 
	 */
	public void enterBatchNumber(String batchNumber) {
		try {
			if (isElementDisplayed(enterBatchNumber)) {
				typeIn(enterBatchNumber, batchNumber);
			} else {
				testStepFailed("The search box to enter the batch number is not displayed");
			}

		} catch (Exception e) {
			testStepFailed("Could not enter batch number in the search box");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click search Icon.
	 * 
	 */
	public void clickSearchIcon() {
		try {
			if (isElementDisplayed(searchIcon)) {
				clickOn(searchIcon);
				clickOn(searchIcon);
			} else {
				testStepFailed("Search Icon not displayed");
			}

		} catch (Exception e) {
			testStepFailed("Could not click on search Icon");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to verify presence of null result image.
	 * 
	 */
	public void verifyNullResult() {
		try {
			waitTime(3);
			if (isElementDisplayed(nullResultImage)) {
				scrollToViewElement(searchIcon);
				highLighterMethod(nullResultImage);
				testStepInfo("Relevant Message displayed for invalid batch Number");
			} else {
				testStepFailed("Relevant Message not displayed for invalid batch Number");
			}

		} catch (Exception e) {
			testStepFailed("Relevant Message not displayed for invalid batch Number");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to check View Results.
	 * 
	 */
	public void checkViewResults() {
		try {
			if (isElementDisplayed(viewResults) && findWebElement(viewResults).isEnabled()) {
				highLighterMethod(viewResults);
				waitTime(2);
				scrollToViewElement(searchIcon);
				testStepInfo("Files were displayed for the search done by the user");
			} else {
				testStepFailed("View Results is not displayed");
			}

		} catch (Exception e) {
			testStepFailed("Files were not displayed for the search done by the user");
			e.printStackTrace();
		}

	}

}
