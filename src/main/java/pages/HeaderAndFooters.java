package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class HeaderAndFooters extends ApplicationKeywords {

	// Header
	private static final String home = "Home #xpath=//span[contains(text(),'Home')]";
	private static final String products = "Products #xpath=//span[contains(text(),'Products')]";
	private static final String products_ShopAll = "Shop All #xpath=//a[contains(text(),'Shop All')]";
	private static final String products_Topicals = "Topicals #xpath=//a[contains(text(),'Topicals')]";
	private static final String products_Edibles = "Edibles #xpath=//li[contains(@class,'level1 ')]/a";
	private static final String products_Edibles_Gummies = "Edibles #xpath=//ul[contains(@class,'dropdown')]//a[contains(@href,'gummies')]";
	private static final String products_Edibles_Capsules = "Edibles #xpath=//ul[contains(@class,'dropdown')]//a[contains(@href,'capsules')]";
	private static final String products_Oils = "Edibles #xpath=//li/a[contains(text(),'Oils')]";
	private static final String labTests = "Edibles #xpath=//span[contains(text(),'Lab Tests')]";
	private static final String contact = "Contact #xpath=//span[contains(text(),'Contact')]/..";
	private static final String miniCart = "MiniCart #xpath=//a/span[contains(text(),'My Cart')]/..";

	// Footer
	private static final String phoneNumber_Footer = "PhoneNumber #xpath=//p[contains(text(),'PHONE:')]";
	private static final String homeFooterLink = "Home Footer Link #xpath=//p[contains(@class,'ft_links')]//a[contains(text(),'HOME')]";
	private static final String ShopFooterLink = "Shop Footer Link #xpath=//p[contains(@class,'ft_links')]//a[contains(text(),'SHOP')]";
	private static final String hempGummiesFooterLink = "HEMP GUMMIES Footer Link #xpath=//p[contains(@class,'ft_links')]//a[contains(text(),'HEMP GUMMIES')]";
	private static final String hempCapsulesFooterLink = "HEMP CAPSULES Footer Link #xpath=//p[contains(@class,'ft_links')]//a[contains(text(),'HEMP CAPSULES')]";
	private static final String hempOilsFooterLink = "HEMP OILS Footer Link #xpath=//p[contains(@class,'ft_links')]//a[contains(text(),'HEMP OILS')]";
	private static final String hempTopicalsFooterLink = "HEMP TOPICALS Footer Link #xpath=//p[contains(@class,'ft_links')]//a[contains(text(),'HEMP TOPICALS')]";
	private static final String sendUsMessageFooterLink = "Send Us a Message #xpath=//a[contains(text(),'Send us a message')]";
	private static final String createAnAccountFooterLink = "Create an account Footer Link #xpath=//a[contains(text(),'CREATE AN ACCOUNT')]";
	private static final String loginFooterLink = "Login Footer Link #xpath=//a[contains(text(),'LOGIN')]";
	private static final String myAccountFooterLink = "MY ACCOUNT Footer Link #xpath=//a[contains(text(),'MY ACCOUNT')]";
	private static final String myOrdersFooterLink = "MY ORDERS Footer Link #xpath=//a[contains(text(),'MY ORDERS')]";
	private static final String shippingFooterLink = "Shipping Footer Link #xpath=//p[contains(@class,'legal_links')]//a[contains(text(),'SHIPPING')]";
	private static final String refundFooterLink = "Refund Footer Link #xpath=//p[contains(@class,'legal_links')]//a[contains(text(),'REFUND')]";
	private static final String termsFooterLink = "Terms Footer Link #xpath=//p[contains(@class,'legal_links')]//a[contains(text(),'TERMS')]";
	private static final String privacyFooterLink = "Privacy Footer Link #xpath=//p[contains(@class,'legal_links')]//a[contains(text(),'PRIVACY')]";
	private static final String ccpaFooterLink = "CCPA Footer Link #xpath=//p[contains(@class,'legal_links')]//a[contains(text(),'CCPA')]";

	private static final String slide_HomePage = "Home Banner Slide #xpath=//div[contains(@id,'slide')]";
	private static final String miniCartEmptyMessage = "Mini Cart Empty #xpath=//span[contains(text(),'You have no items in your shopping cart.')]";
	private static final String miniCartSubtotalHeader = "Mini Cart Subtotal Header #xpath=//span[contains(text(),'Cart Subtotal')]";
	private static final String shippingAddressHeader = "Shipping Address Header #xpath=//div[contains(text(),'Shipping Address')]";
	private static final String emptyCartMessage = "Empty Cart Message #xpath=//p[contains(text(),'You have no items in your shopping cart.')]";
	private static final String shopAllHeader = "Shop All Header #xpath=//h1[contains(text(),'Shop All')]";
	private static final String topicalsHeader = "Topicals Header #xpath=//h1[contains(text(),'Topicals')]";
	private static final String ediblesHeader = "Edibles Header #xpath=//h1[contains(text(),'Edibles')]";
	private static final String gummiesHeader = "Gummies Header #xpath=//h1[contains(text(),'Gummies')]";
	private static final String capsulesHeader = "CBD Paw Butter Header #xpath=//h1[contains(text(),'Capsules')]";
	private static final String oilsHeader = "Oils Header #xpath=//h1[contains(text(),'Oils')]";
	private static final String labTestsHeader = "Lab Tests Header #xpath=//h1[contains(text(),'Lab Tests')]";
	private static final String contactUsHeader = "Contact Us Header #xpath=//h2[contains(text(),'How can we help you?')]";
	private static final String loginHeader = "Contact Us Header #xpath=//div[contains(text(),'Login Your Account')]";
	private static final String registerHeader = "Contact Us Header #xpath=";
	private static final String myAccountHeader = "Contact Us Header #xpath=";
	private static final String myOrdersHeader = "Contact Us Header #xpath=";

	private static final String shippingPolicyHeader = "Shipping Policy Header #xpath=//h1[contains(text(),'Shipping Policy')]";
	private static final String refundPolicyHeader = "Relief Policy Header #xpath=//h1[contains(text(),'Refund Policy')]";
	private static final String termsConditionsHeader = "Terms and Conditions Header #xpath=//h1[contains(text(),'Terms and Conditions')]";
	private static final String privacyPolicyHeader = "Privacy Policy Header #xpath=//h1[contains(text(),'Privacy Policy')]";
	private static final String ccpaHeader = "CCPA Header Header #xpath=//h1[contains(text(),'CCPA Data Request')]";

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to click on home in Header.
	 */
	public void goTo_Home() {

		try {
			if (isElementPresent(home)) {
				highLighterMethod(home);
				clickOn(home);
			} else {
				testStepFailed("Home not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Home could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method to verify presence of Phone Number
	 */
	public void Verifypresence_PhoneNumber() {
		try {
			if (isElementPresent(phoneNumber_Footer)) {
				scrollToViewElement(phoneNumber_Footer);
				String phoneNumber = getText(phoneNumber_Footer);
				if (findWebElement(phoneNumber_Footer).isDisplayed() && phoneNumber.contains("833-973-1497")) {
					highLighterMethod(phoneNumber_Footer);
					testStepInfo("The Number present in Call Us is : " + "833-973-1497");
				}
			} else {
				testStepFailed("Phone Number is not present in Footer");
			}
		} catch (Exception e) {
			testStepFailed("Presence of Phone Number in Footer could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Mini cart Icon in Header
	 */
	public void clickOnMiniCart() {

		try {
			if (isElementDisplayed(miniCart)) {
				highLighterMethod(miniCart);
				clickOn(miniCart);
			} else {
				testStepFailed("Mini Cart not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Mini Cart could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on desired option from Products menu
	 */
	public void goTo_Products(int subMenu, int subMenu_2) {

		try {
			if (subMenu == 0) {
				if (isElementPresent(products)) {
					highLighterMethod(products);
					clickOn(products);
				} else {
					testStepFailed("Products not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(products_ShopAll)) {
						navigateMenu_Two(products, products_ShopAll);
					} else {
						testStepFailed("Shop All not present under products");
					}

					break;
				}

				case 2: {
					if (isElementPresent(products_Topicals)) {
						navigateMenu_Two(products, products_Topicals);
					} else {
						testStepFailed("Topicals not present under products");
					}

					break;
				}

				case 3: {
					if (subMenu_2 == 0) {
						if (isElementPresent(products_Edibles)) {
							navigateMenu_Two(products, products_Edibles);
						} else {
							testStepFailed("Edibles not present under products");
						}
						break;
					} else {
						switch (subMenu_2) {
						case 1: {
							if (isElementPresent(products_Edibles_Gummies)) {
								navigateMenu_Three(products, products_Edibles, products_Edibles_Gummies);
							} else {
								testStepFailed("Gummies not present under Edibles in Shop Menu");
							}
							break;
						}

						case 2: {
							if (isElementPresent(products_Edibles_Capsules)) {
								navigateMenu_Four(products, products_Edibles, products_Edibles_Gummies,
										products_Edibles_Capsules);
							} else {
								testStepFailed("Capsules not present under Edibles in Shop Menu");
							}
							break;
						}
						}
					}
					break;
				}
				case 4: {
					if (isElementPresent(products_Oils)) {
						navigateMenu_Two(products, products_Oils);
					} else {
						testStepFailed("Oils not present under products");
					}

					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the Shop menu could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on About Us in Header.
	 */
	public void goTo_Labtests() {

		try {
			if (isElementPresent(labTests)) {
				highLighterMethod(labTests);
				clickOn(labTests);
			} else {
				testStepFailed("Lab Tests not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Lab Tests could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on contact
	 */
	public void goTo_Contact() {

		try {
			if (isElementPresent(contact)) {
				highLighterMethod(contact);
				clickOn(contact);
			} else {
				testStepFailed("Contact not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Contact could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify the routing.
	 */
	public void verifyNavigation(String option) {

		try {
			switch (option) {
			case "home": {
				waitForElementToDisplay(slide_HomePage, 8);
				if (isElementDisplayed(slide_HomePage)) {
					highLighterMethod(slide_HomePage);
					scrollToViewElement(slide_HomePage);
					testStepInfo("The relevant page was opened upon clicking Home");
				} else {
					testStepFailed("Home Page navigation could not be verfied", "The Home Page Slider not present");
				}
				break;
			}

			case "emptyMiniCart": {
				if (isElementDisplayed(miniCartEmptyMessage)) {
					highLighterMethod(miniCartEmptyMessage);
					testStepInfo("The empty cart message was displayed in the Mini Cart");
				} else {
					testStepFailed("The empty cart message was not displayed in the Mini Cart");
				}
				break;
			}

			case "miniCartWithProducts": {
				if (isElementDisplayed(miniCartSubtotalHeader)) {
					highLighterMethod(miniCartSubtotalHeader);
					testStepInfo("Mini cart with a product opened successfully");
				} else {
					testStepFailed("Mini cart with a product opened successfully",
							"Mini Cart Subtotal Header not displayed");
				}
				break;
			}

			case "emptyCart": {
				if (isElementDisplayed(emptyCartMessage)) {
					highLighterMethod(emptyCartMessage);
					testStepInfo("Empty Cart message was displayed in the Shopping Cart page");
				} else {
					testStepFailed("Empty Cart message was not displayed in the Shopping Cart page");
				}
				break;
			}

			case "checkout": {
				if (isElementDisplayed(shippingAddressHeader)) {
					highLighterMethod(shippingAddressHeader);
					testStepInfo("The checkout page was displayed");
				} else {
					testStepFailed("he checkout page was not displayed", "Shipping Address Header was not displayed");
				}
				break;
			}

			case "shopAll": {
				if (isElementDisplayed(shopAllHeader)) {
					highLighterMethod(shopAllHeader);
					testStepInfo("ShopAll page was opened successfully");
				} else {
					testStepFailed("ShopAll page could not be opened successfully",
							"Shop All Header was not displayed");
				}
				break;
			}

			case "topicals": {
				if (isElementDisplayed(topicalsHeader)) {
					highLighterMethod(topicalsHeader);
					testStepInfo("The Topicals Page was opened");
				} else {
					testStepFailed("The Topicals Page could not be opened", "Topicals Header was not displayed");
				}
				break;
			}

			case "edibles": {
				if (isElementDisplayed(ediblesHeader)) {
					highLighterMethod(ediblesHeader);
					testStepInfo("Edibles page was opened successfully");
				} else {
					testStepFailed("Edibles page could not be opened successfully", "Edibles Header was not displayed");
				}
				break;
			}

			case "gummies": {
				if (isElementDisplayed(gummiesHeader)) {
					highLighterMethod(gummiesHeader);
					testStepInfo("Gummies page was opened successfully");
				} else {
					testStepFailed("Gummies page could not be opened successfully", "Gummies Header was not displayed");
				}
				break;
			}

			case "capsules": {
				if (isElementDisplayed(capsulesHeader)) {
					highLighterMethod(capsulesHeader);
					testStepInfo("Capsules page was opened successfully");
				} else {
					testStepFailed("Capsules page could not be opened successfully",
							"Capsules Header was not displayed");
				}
				break;
			}
			case "oils": {
				if (isElementDisplayed(oilsHeader)) {
					highLighterMethod(oilsHeader);
					testStepInfo("Oils page was opened successfully");
				} else {
					testStepFailed("Oils page could not be opened successfully", "Oils Header was not displayed");
				}
				break;
			}

			case "labTests": {
				if (isElementDisplayed(labTestsHeader)) {
					highLighterMethod(labTestsHeader);
					testStepInfo("Lab Tests page was opened successfully");
				} else {
					testStepFailed("Lab Tests page could not be opened successfully",
							"Lab Tests Header was not displayed");
				}
				break;
			}

			case "contact": {
				if (isElementDisplayed(contactUsHeader)) {
					highLighterMethod(contactUsHeader);
					testStepInfo("Contact page was opened successfully");
				} else {
					testStepFailed("Contact page could not be opened successfully", "Contact Header was not displayed");
				}
				break;
			}

			case "login": {
				if (isElementDisplayed(loginHeader)) {
					highLighterMethod(loginHeader);
					testStepInfo("Login page was opened successfully");
				} else {
					testStepFailed("Login page could not be opened successfully", "Login Header was not displayed");
				}
				break;
			}

			case "register": {
				if (isElementDisplayed(registerHeader)) {
					highLighterMethod(registerHeader);
					testStepInfo("Register page was opened successfully");
				} else {
					testStepFailed("Register page could not be opened successfully",
							"Register Header was not displayed");
				}
				break;
			}

			case "myAccount": {
				if (isElementDisplayed(myAccountHeader)) {
					highLighterMethod(myAccountHeader);
					testStepInfo("My Account page was opened successfully");
				} else {
					testStepFailed("My Account page could not be opened successfully",
							"My Account Header was not displayed");
				}
				break;
			}

			case "myOrders": {
				if (isElementDisplayed(myOrdersHeader)) {
					highLighterMethod(myOrdersHeader);
					testStepInfo("My Orders page was opened successfully");
				} else {
					testStepFailed("My Orders page could not be opened successfully",
							"My Orders Header was not displayed");
				}
				break;
			}

			case "shipping": {
				if (isElementDisplayed(shippingPolicyHeader)) {
					highLighterMethod(shippingPolicyHeader);
					testStepInfo("Shipping page was opened successfully");
				} else {
					testStepFailed("Shipping page could not be opened successfully",
							"Shipping Header was not displayed");
				}
				break;
			}

			case "refund": {
				if (isElementDisplayed(refundPolicyHeader)) {
					highLighterMethod(refundPolicyHeader);
					testStepInfo("Refund page was opened successfully");
				} else {
					testStepFailed("Refund page could not be opened successfully", "Refund Header was not displayed");
				}
				break;
			}

			case "terms": {
				if (isElementDisplayed(termsConditionsHeader)) {
					highLighterMethod(termsConditionsHeader);
					testStepInfo("Terms page was opened successfully");
				} else {
					testStepFailed("Terms page could not be opened successfully", "Terms Header was not displayed");
				}
				break;
			}

			case "privacy": {
				if (isElementDisplayed(privacyPolicyHeader)) {
					highLighterMethod(privacyPolicyHeader);
					testStepInfo("Privacy page was opened successfully");
				} else {
					testStepFailed("Privacy page could not be opened successfully", "Privacy Header was not displayed");
				}
				break;
			}

			case "ccpa": {
				if (isElementDisplayed(ccpaHeader)) {
					highLighterMethod(ccpaHeader);
					testStepInfo("The relevant page was opened upon clicking CCPA in Footer");
					driver.navigate().back();
				} else {
					testStepFailed("The relevant page was not opened upon clicking CCPA in Footer",
							"CCPA Header was not displayed");
				}
				break;
			}

			}
		} catch (Exception e) {
			testStepFailed("Relevant verification could not be done successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to navigate through Footer.
	 */
	public void navigateFooter(String option) {

		try {
			switch (option) {
			case "home": {
				if (isElementDisplayed(homeFooterLink)) {
					highLighterMethod(homeFooterLink);
					clickOn(homeFooterLink);
				} else {
					testStepFailed("Home not displayed in Footer");
				}
				break;
			}

			case "shop": {
				if (isElementPresent(ShopFooterLink)) {
					highLighterMethod(ShopFooterLink);
					clickOn(ShopFooterLink);
				} else {
					testStepFailed("Shop not displayed in Footer");
				}
				break;
			}

			case "hempGummies": {
				if (isElementPresent(hempGummiesFooterLink)) {
					highLighterMethod(hempGummiesFooterLink);
					clickOn(hempGummiesFooterLink);
				} else {
					testStepFailed("Hemp Gummies was not displayed in Footer");
				}
				break;
			}

			case "hempCapsules": {
				if (isElementPresent(hempCapsulesFooterLink)) {
					highLighterMethod(hempCapsulesFooterLink);
					clickOn(hempCapsulesFooterLink);
				} else {
					testStepFailed("Hemp Capsules was not displayed in Footer");
				}
				break;
			}

			case "hempOils": {
				if (isElementPresent(hempOilsFooterLink)) {
					highLighterMethod(hempOilsFooterLink);
					clickOn(hempOilsFooterLink);
				} else {
					testStepFailed("Hemp Oils was not displayed in Footer");
				}
				break;
			}

			case "hempTopicals": {
				if (isElementPresent(hempTopicalsFooterLink)) {
					highLighterMethod(hempTopicalsFooterLink);
					clickOn(hempTopicalsFooterLink);
				} else {
					testStepFailed("Hemp Topicals was not displayed in Footer");
				}
				break;
			}

			case "createAnAccount": {
				if (isElementPresent(createAnAccountFooterLink)) {
					highLighterMethod(createAnAccountFooterLink);
					clickOn(createAnAccountFooterLink);
				} else {
					testStepFailed("Create An Account was not displayed in Footer");
				}
				break;
			}

			case "login": {
				if (isElementPresent(loginFooterLink)) {
					highLighterMethod(loginFooterLink);
					clickOn(loginFooterLink);
				} else {
					testStepFailed("Login was not displayed in Footer");
				}
				break;
			}

			case "myAccount": {
				if (isElementPresent(myAccountFooterLink)) {
					highLighterMethod(myAccountFooterLink);
					clickOn(myAccountFooterLink);
				} else {
					testStepFailed("My Account was not displayed in Footer");
				}
				break;
			}

			case "myOrders": {
				if (isElementPresent(myOrdersFooterLink)) {
					highLighterMethod(myOrdersFooterLink);
					clickOn(myOrdersFooterLink);
				} else {
					testStepFailed("My Orders was not displayed in Footer");
				}
				break;
			}

			case "sendUsMessage": {
				if (isElementPresent(sendUsMessageFooterLink)) {
					highLighterMethod(sendUsMessageFooterLink);
					clickOn(sendUsMessageFooterLink);
				} else {
					testStepFailed("'Send Us A Message' not displayed in Footer");
				}
				break;
			}

			case "shipping": {
				if (isElementPresent(shippingFooterLink)) {
					highLighterMethod(shippingFooterLink);
					clickOn(shippingFooterLink);
				} else {
					testStepFailed("Shipping not displayed in Footer");
				}
				break;
			}

			case "refund": {
				if (isElementPresent(refundFooterLink)) {
					highLighterMethod(refundFooterLink);
					clickOn(refundFooterLink);
				} else {
					testStepFailed("Refund not displayed in Footer");
				}
				break;
			}

			case "terms": {
				if (isElementPresent(termsFooterLink)) {
					highLighterMethod(termsFooterLink);
					clickOn(termsFooterLink);
				} else {
					testStepFailed("Terms not displayed in Footer");
				}
				break;
			}

			case "privacy": {
				if (isElementPresent(privacyFooterLink)) {
					highLighterMethod(privacyFooterLink);
					clickOn(privacyFooterLink);
				} else {
					testStepFailed("Privacy not displayed in Footer");
				}
				break;
			}

			case "ccpa": {
				if (isElementPresent(ccpaFooterLink)) {
					highLighterMethod(ccpaFooterLink);
					clickOn(ccpaFooterLink);
				} else {
					testStepFailed("CCPA not displayed in Footer");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Requested navigation in footer could not be done");
			e.printStackTrace();
		}
	}
}
