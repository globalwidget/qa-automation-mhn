package pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Cart_AllProducts extends ApplicationKeywords {

	private static final String getAllProducts = "All Products #xpath=//div[@class='product-item-info']//span[@class='product-image-container']/..";
	private static final String title_selectedProduct_PGP = "All Products #xpath=//span[@class='product-image-container']/../../following-sibling::div//a";
	private static final String addToCart = "Add To Cart #xpath=//button[contains(text(),'Add To Cart')]";
	private static final String selectCapsuleCount = "Select Capsule Count #xpath=//select[contains(@id,'attribute')]";
	private static final String proceedToCart = "Proceed To Cart #xpath=//button[contains(text(),'Proceed to Cart')]";
	private static final String title_selectedProduct_PDP = "Title of product selected #xpath=//h1[contains(@class,'title')]";
	private static final String CloseProceedToCart = "Close Proceed To Cart Pop Up #xpath=//aside[contains(@class,'viewBox ')]//header/button[contains(@class,'close')]";
	private static final String quantityUpdateBox = "Quantity Update Box #xpath=//div[@class != 'qty-box']/input[@type='number']";
	private static final String productPrice = "Product Price #xpath=//span[contains(@id,'product-price')]/span";
	private static final String AddToCartPopUp_ProductName = "Product Price #xpath=//div[@class='product-confirm-name']";
	private static final String AddToCartPopUp_Quantity = "Product Price #xpath=//div[contains(@class,'product-confirm-qty')]/span[@class='value']";
	private static final String AddToCartPopUp_CartSubTotal = "Cart Subtotal #xpath=//span[@class='value']/span[@class='price']";

	public Cart_AllProducts(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to select the first product on the page
	 */
	public void selectFirstProduct() {
		try {
			waitForElementToDisplay(getAllProducts, 10);
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				scrollToViewElement(allProducts.get(0));
				waitTime(3);
				allProducts.get(0).click();
				testStepInfo("The first product is clicked");
			} else {
				testStepFailed("Products were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("First Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get product name in PGP Page
	 */
	public String getProductName_PGP() {
		try {
			if (isElementDisplayed(title_selectedProduct_PGP)) {
				java.util.List<WebElement> productNames = new ArrayList<WebElement>();
				productNames = findWebElements(title_selectedProduct_PGP);
				scrollToViewElement(productNames.get(0));
				String value = productNames.get(0).getText();
				return value;
			} else {
				testStepFailed("Could not get the product name from PGP Page", "Element not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product name from PGP Page");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to select the second product on the page
	 */
	public void selectSecondProduct() {
		try {
			waitForElementToDisplay(getAllProducts, 10);
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				scrollToViewElement(allProducts.get(1));
				waitTime(3);
				allProducts.get(1).click();
				testStepInfo("The second product is clicked");
			} else {
				testStepFailed("Products were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Second Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to choose capsule Strength
	 */
	public void chooseCapsuleStrength() {
		try {
			if (findWebElements(selectCapsuleCount).size() > 0) {
				if (isElementDisplayed(selectCapsuleCount)) {
					highLighterMethod(selectCapsuleCount);
					selectFromDropdown(selectCapsuleCount, 1);
					testStepInfo("First option under select capsule Count was selected");
				} else {
					testStepFailed("Select Capsule Count - was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Could not select flavor and strength");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Cart
	 */
	public void clickAddToCart() {
		try {
			if (isElementDisplayed(addToCart)) {
				highLighterMethod(addToCart);
				clickOn(addToCart);
				testStepInfo("The add to cart button was clicked");
			} else {
				testStepFailed("The add to cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on View Cart
	 */
	public void clickProceedToCart() {
		try {
			highLighterMethod(proceedToCart);
			if (isElementDisplayed(proceedToCart)) {
				clickOn(proceedToCart);
				GOR.productAdded = true;
				testStepInfo("The Proceed to Cart button was clicked");
			} else {
				testStepFailed("The Proceed to Cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Proceed to cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on View Cart
	 */
	public void closeProceedToCartPopUp() {
		try {
			waitForElementToDisplay(CloseProceedToCart, 40);
			highLighterMethod(CloseProceedToCart);
			if (isElementDisplayed(CloseProceedToCart)) {
				clickOn(CloseProceedToCart);
				GOR.productAdded = true;
				waitForElementToDisplay(addToCart, 10);
				testStepInfo("The Proceed to Cart Pop Up was closed");
			} else {
				testStepFailed("The Proceed to Cart Pop Up was not closed");
			}
		} catch (Exception e) {
			testStepFailed("Proceed to cart Pop up could not be closed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get title of the product after selecting it from PGP
	 * Page.
	 */
	public String getProductTitle_PDP() {
		try {
			if (isElementDisplayed(title_selectedProduct_PDP)) {
				highLighterMethod(title_selectedProduct_PDP);
				return getText(title_selectedProduct_PDP);
			} else {
				testStepFailed("Product Title of the selected product not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product title");
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Description: Method to update product quantity.
	 */
	public void updateProductQuantity(int quantity) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				typeIn(quantityUpdateBox, String.valueOf(quantity));
			} else {
				testStepFailed("Could not update the quantity of the product in the PDP page",
						"Quantity update box not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not update the quantity of the product in the PDP page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get product price.
	 */
	public float getProductPrice() {
		try {
			waitForElementToDisplay(productPrice, 10);
			if (isElementDisplayed(productPrice)) {
				highLighterMethod(productPrice);
				String price = getText(productPrice).substring(1);
				float productPrice = Float.parseFloat(price);
				return productPrice;
			} else {
				testStepFailed("Could not get the price of the product in the PDP page", "Product Price not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the price of the product in the PDP page");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description: Method to check the details of the Add To Cart Pop Up.
	 */
	public void checkDetails_AddToCart(String productName, int quantity, float cartSubTotal) {
		try {
			waitForElementToDisplay(AddToCartPopUp_ProductName, 10);
			if (isElementDisplayed(AddToCartPopUp_ProductName)) {
				if (getText(AddToCartPopUp_ProductName).toLowerCase().contains(productName.toLowerCase())) {
					highLighterMethod(AddToCartPopUp_ProductName);
					testStepInfo("Product Name in the Add To Cart Pop Up is verified");
				} else {
					testStepFailed("Product Name in the Add To Cart Pop Up could not be verified");
				}
				if (Integer.parseInt(getText(AddToCartPopUp_Quantity)) == quantity) {
					highLighterMethod(AddToCartPopUp_Quantity);
					testStepInfo("Quantity in the Add To Cart Pop Up is verified");
				} else {
					testStepFailed("Quantity in the Add To Cart Pop Up could not be verified");
				}
				if (Float.parseFloat(getText(AddToCartPopUp_CartSubTotal).substring(1)) == cartSubTotal) {
					highLighterMethod(AddToCartPopUp_CartSubTotal);
					testStepInfo("Cart Subtotal in the Add To Cart Pop Up is verified");
				} else {
					testStepFailed("Cart Subtotal in the Add To Cart Pop Up could not be verified");
				}
			} else {
				testStepFailed("Product Name not displayed in the Add To Cart Pop Up");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify the details in the Add To Cart Pop Up");
			e.printStackTrace();
		}
	}
}
