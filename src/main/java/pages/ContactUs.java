package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class ContactUs extends ApplicationKeywords {

	private static final String name = "Name #id=name";
	private static final String phone = "Phone #id=contact-phone";
	private static final String email = "Email #id=contact-email";
	private static final String business = "Email #id=contact-business";
	private static final String yourMessage = "Your Message #id=comment";
	private static final String sendMessage = "Send #xpath=//span[contains(text(),'Send Message')]/..";

	private static final String name_EmptyError = "First Name Empty Error #xpath=//div[@id='name-error' and contains(text(),'This is a required field.')]";
	private static final String phone_EmptyError = "Phone Empty Error #xpath=//div[@id='contact-phone-error' and contains(text(),'This is a required field.')]";
	private static final String email_EmptyError = "Email Empty Error #xpath=//div[@id='contact-email-error' and contains(text(),'This is a required field.')]";
	private static final String yourMessage_EmptyError = "Your Message Empty Error #xpath=//div[@id='comment-error' and contains(text(),'This is a required field.')]";
	private static final String EmailAddressInvalidError = "Email Address Invalid Error #xpath=//div[contains(text(),'Please enter a valid email address')]";
	private static final String phoneInvalidError = "Telephone Invalid Error #xpath=//div[contains(text(),'Please enter more or equal than 9 symbols')]";
	private static final String successMessage = "Success Message #xpath=//div[contains(text(),'Thanks for contacting us with your comments and questions')]";

	public ContactUs(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to enter details in the form
	 * 
	 */
	public void fillDetails(String name_data, String phone_data, String email_data, String business_data,
			String message_data) {
		try {
			if (isElementDisplayed(name) && !(name_data.isEmpty())) {
				highLighterMethod(name);
				typeIn(name, name_data);
			}
			if (isElementDisplayed(phone) && !(phone_data.isEmpty())) {
				highLighterMethod(phone);
				typeIn(phone, phone_data);
			}
			if (isElementDisplayed(email) && !(email_data.isEmpty())) {
				highLighterMethod(email);
				typeIn(email, email_data);
			}
			if (isElementDisplayed(business) && !(business_data.isEmpty())) {
				highLighterMethod(business);
				typeIn(business, business_data);
			}
			if (isElementDisplayed(yourMessage) && !(message_data.isEmpty())) {
				highLighterMethod(yourMessage);
				typeIn(yourMessage, message_data);
			}
		} catch (Exception e) {
			testStepFailed("Could not fill details successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Send Message
	 */
	public void clickSend() {
		try {
			if (isElementDisplayed(sendMessage)) {
				highLighterMethod(sendMessage);
				clickOn(sendMessage);
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Send Message");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check for all empty field errors together.
	 */
	public void checkEmptyFieldErrors() {
		try {
			if (isElementDisplayed(name_EmptyError) && isElementDisplayed(phone_EmptyError)
					&& isElementDisplayed(email_EmptyError) && isElementDisplayed(yourMessage_EmptyError)) {
				highLighterMethod(name_EmptyError);
				highLighterMethod(phone_EmptyError);
				highLighterMethod(email_EmptyError);
				highLighterMethod(yourMessage_EmptyError);
				testStepInfo("All Empty Field Errors were displayed");
			} else
				testStepFailed("All Empty Field Errors were not displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate empty field errors successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check for invalid email and invalid phone Errors.
	 */
	public void checkInvalidDataErrors() {
		try {
			if (isElementDisplayed(EmailAddressInvalidError)) {
				highLighterMethod(EmailAddressInvalidError);
				testStepInfo("Invalid Email Error was displayed");
			} else
				testStepFailed("Invalid Email Error was not displayed");
			if (isElementDisplayed(phoneInvalidError)) {
				highLighterMethod(phoneInvalidError);
				testStepInfo("Invalid Phone Number Error was displayed");
			} else
				testStepFailed("Invalid Phone Number Error was not displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate Invalid email and Invalid phone nummber errors successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Success Message.
	 */
	public void verifySuccessMessage() {
		try {
			waitForElementToDisplay(successMessage, 10);
			if (isElementDisplayed(successMessage)) {
				highLighterMethod(successMessage);
				testStepInfo("Success Message was displayed");
			} else
				testStepFailed("Success Message was not displayed");
		} catch (Exception e) {
			testStepFailed("Sucess Message could not be validated successfully");
			e.printStackTrace();
		}
	}

}
