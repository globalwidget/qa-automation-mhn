package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class ShoppingCart extends ApplicationKeywords {

	private static final String productsPresent = "Products Present #xpath=//strong[contains(@class,'product')]/a";
	private static final String removeProduct = "Remove Product #xpath=//span[contains(text(),'Remove item')]/..";
	private static final String quantityUpdateBox = "Quantity Update Box #xpath=//input[@type='number']";
	private static final String updateCart = "Update Cart button #xpath=//span[contains(text(),'Update Cart')]";
	private static final String clearCart = "Clear Shopping Cart button #xpath=//span[contains(text(),'Clear Shopping Cart')]/..";
	private static final String continueShopping = "Continue Shopping button #xpath=//span[contains(text(),'Continue Shopping')]/..";
	private static final String proceedCheckout = "Proceed Checkout #xpath=//span[contains(text(),'Proceed to Checkout')]/..";
	private static final String shoppingCartHeader = "Shopping Cart Header #xpath=//h1/span[contains(text(),'Shopping Cart')]";
	private static final String country_Shipping = "Shipping Country #xpath=//select[@name='country_id']";
	private static final String state_Shipping = "Shipping State #xpath=//select[@name='region_id']";
	private static final String postalCode_Shipping = "Postal Code #xpath=//input[@name='postcode']";
	private static final String standardRadiobox_Shipping = "Standard Shiiping Radio button #xpath=//input[@id='s_method_flatrate_flatrate']";
	private static final String cartSubtotalElements_Shipping = "Cart Subtotal elements #xpath=//tr[@class='totals shipping excl']/following-sibling::tr";

	public ShoppingCart(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of the expected product in the cart
	 * 
	 */
	public void verifyPresenceOfExpectedProduct(String productName) {
		try {
			boolean productPresent = false;
			if (isElementPresent(productsPresent)) {
				manualScreenshot("The Product added is present in the cart");
				List<WebElement> products = new ArrayList<WebElement>();
				products = findWebElements(productsPresent);
				for (WebElement product : products) {
					if (product.getText().toLowerCase().contains(productName.toLowerCase())) {
						productPresent = true;
						highLighterMethod(product);
						break;
					}
				}
				if (productPresent == true) {
					testStepInfo("The product added to cart " + productName + ", found in My Cart page");
				} else
					testStepFailed("Product added to cart " + productName + ", not found in My Cart page");
			} else
				testStepFailed("No products present in My cart page");
		} catch (Exception e) {
			testStepFailed("Presence of expected product could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on delete button in My Cart page
	 */
	public String clickOnRemoveProduct() {
		String productName = "";
		try {
			if (isElementDisplayed(removeProduct)) {
				highLighterMethod(removeProduct);
				productName = findWebElements(productsPresent).get(0).getText();
				findWebElements(removeProduct).get(0).click();
				GOR.productAdded = false;
				testStepInfo("The remove button was clicked for the product " + productName);
				return productName;
			} else
				testStepFailed("Could not find any product in My Cart");
		} catch (Exception e) {
			testStepFailed("Could not remove products from cart");
			e.printStackTrace();
		}
		return productName;
	}

//	/**
//	 * Description: Method to verify expected message is displayed in the Alert
//	 */
//	public void verifyExpectedMessageAlert(String message1, String message2, String message3) {
//		try {
//			if (isElementDisplayed(alertMessage)) {
//				highLighterMethod(alertMessage);
//				scrollUp();
//				manualScreenshot("Alert message displayed");
//				String value = getText(alertMessage);
//				if (value.contains(message1) && value.contains(message2) && value.contains(message3))
//					testStepInfo("Relevant Alert Message was displayed");
//				else
//					testStepFailed("Relevant Alert Message was not displayed");
//
//			} else
//				testStepFailed("No Alert message was displayed");
//		} catch (Exception e) {
//			testStepFailed("Could not validate expected Messages");
//			e.printStackTrace();
//		}
//	}

	/**
	 * Description: Method to update the cart quantity of the first product in the
	 * cart
	 */
	public void updateCartQuantity(int quantity) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				findWebElements(quantityUpdateBox).get(0).clear();
				findWebElements(quantityUpdateBox).get(0).sendKeys(String.valueOf(quantity));
			} else
				testStepFailed("The box to update quantity of the product is not found");
		} catch (Exception e) {
			testStepFailed("Could not update quantity of the product");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to update the cart quantity of the first product in the
	 * cart
	 */
	public void checkCartQuantity(int number) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				int quantity = Integer.parseInt(getAttributeValue(quantityUpdateBox, "value"));
				if (quantity == number) {
					testStepInfo("The quantity expected is present");
				}
			} else
				testStepFailed("The box to update quantity of the product is not found");
		} catch (Exception e) {
			testStepFailed("The quantity expected is not present");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Update Cart
	 */
	public void clickUpdateCart() {
		try {
			if (isElementDisplayed(updateCart)) {
				highLighterMethod(updateCart);
				clickOn(updateCart);
				waitTime(3);
				waitForElementToDisplay(updateCart, 10);
			} else
				testStepFailed("The Update Cart button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Update Cart button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Proceed to Checkout
	 */
	public void clickProceedToCheckOut() {
		try {
			if (isElementDisplayed(proceedCheckout)) {
				highLighterMethod(proceedCheckout);
				clickOn(proceedCheckout);
			} else
				testStepFailed("Proceed to Checkout was not displayed");
		} catch (Exception e) {
			testStepFailed("Proceed to checkout could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get the product quantity
	 */
	public int getProductQuantity() {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				int quantity = Integer.parseInt(getAttributeValue(quantityUpdateBox, "value"));
				return quantity;
			} else
				testStepFailed("Could not get the quantity of the product", "Quantity update box is not displayed");
		} catch (Exception e) {
			testStepFailed("Could not get the quantity of the product");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description: Method to verify Shopping Cart page navigation.
	 */
	public void verifyShoppingCartHeader() {
		try {
			if (isElementDisplayed(shoppingCartHeader)) {
				highLighterMethod(shoppingCartHeader);
				testStepInfo("Shopping Cart page opened successfully");
			} else {
				testStepFailed("Shopping Cart page was not opened successfully",
						"Shopping Cart Header was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Shopping Cart page was not opened successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Clear Cart
	 */
	public void clickClearCart() {
		try {
			if (isElementDisplayed(clearCart)) {
				highLighterMethod(clearCart);
				clickOn(clearCart);
				waitTime(3);
			} else
				testStepFailed("The Clear Cart button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Clear Cart button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Continue Shopping button
	 */
	public void clickContinueShoppingCart() {
		try {
			if (isElementDisplayed(continueShopping)) {
				highLighterMethod(continueShopping);
				clickOn(continueShopping);
				waitTime(3);
				waitForElementToDisplay(continueShopping, 10);
			} else
				testStepFailed("The Continue Shopping button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Continue Shopping button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to fill certain Shipping details in cart page
	 */
	public void fillDetialsShipping(String state, int zip) {
		try {
			scrollToViewElement(country_Shipping);
			if (isElementDisplayed(country_Shipping)) {
				if (isElementDisplayed(state_Shipping) && state != null) {
//					if (state.contains("Florida")) {
//						highLighterMethod(state_Shipping);
//						selectFromDropdown(state_Shipping, 18);
//						selectFromDropdown(state_Shipping, "Florida");
//					}
//					if (state.contains("California")) {
//						highLighterMethod(state_Shipping);
//						selectFromDropdown(state_Shipping, 12);
//						selectFromDropdown(state_Shipping, "California");
//					}
					highLighterMethod(state_Shipping);
					selectFromDropdown(state_Shipping, state);
				}
				if (isElementDisplayed(postalCode_Shipping) && zip != 0) {
					highLighterMethod(postalCode_Shipping);
					typeIn(postalCode_Shipping, String.valueOf(zip));
				}
				if (isElementDisplayed(standardRadiobox_Shipping)) {
					highLighterMethod(standardRadiobox_Shipping);
					clickOn(standardRadiobox_Shipping);
				}
			} else
				testStepFailed("Shipping Country dropdown is not displayed");
		} catch (Exception e) {
			testStepFailed("Could not fill the shipping details in the cart page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check presence or absence of tax in the sub-total
	 */
	public void checkTax(boolean presence) {
		try {
			boolean taxPresent = false;
			List<WebElement> cartSutotal = new ArrayList<WebElement>();
			cartSutotal = findWebElements(cartSubtotalElements_Shipping);
			for (WebElement element : cartSutotal) {
				if (element.getAttribute("class").contains("totals-tax"))
					taxPresent = true;
			}
			if (presence == true) {
				if (taxPresent == true) {
					highLighterMethod(cartSubtotalElements_Shipping);
					testStepInfo("Tax was present in the cart subtotal as expected");
				} else {
					testStepFailed("Tax was not present in the cart subtotal as expected");
				}
			} else if (presence == false) {
				if (taxPresent == false) {
					highLighterMethod(cartSubtotalElements_Shipping);
					testStepInfo("Tax was not present in the cart subtotal as expected");
				} else {
					testStepFailed("Tax was present in the cart subtotal");
				}
			}
		} catch (Exception e) {
			testStepFailed("Presence or Absence of tax could not be checked");
			e.printStackTrace();
		}
	}

}
